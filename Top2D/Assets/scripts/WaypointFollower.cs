using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollower : MonoBehaviour
{

    [SerializeField] private GameObject[] waypoint;
    //[SerializeField] private float speed = 2f;
    private int currIndex = 0;
    private bool isMoving = true;
    private float timeToWait = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(transform.position, waypoint[currIndex].transform.position) < 0.1f)
        {
            currIndex++;
            if (currIndex > waypoint.Length)
            {
                currIndex = 0;
            }

        }
        if (isMoving == true) { 
            transform.position = Vector2.MoveTowards(transform.position, waypoint[currIndex].transform.position, Time.deltaTime);
        } else {
            timeToWait = timeToWait - Time.deltaTime;
            if (timeToWait <= 0 )
                isMoving = true;
        }
    }


    public void WaitGodDammit(float sek) {
        isMoving = false;
        timeToWait = sek;
    }
}
