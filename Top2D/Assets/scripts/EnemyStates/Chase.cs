using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : BaseState
{
    public Transform transform;
    GameObject player;
    private float speed = 0.3f;
    public Chase(EnemySM stateMachine) : base("Chase", stateMachine)
    {
        transform = stateMachine.GetTransform();
        player = GameObject.FindGameObjectWithTag("Player");
    }
    public override void Enter()
    {
        base.Enter();
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();

         if (Vector2.Distance(transform.position, player.transform.position) < 0.1f)
        {
            Debug.Log("time to change state...");
            stateMachine.ChangeState(((EnemySM)stateMachine).st_attack);
        }

        if (Vector2.Distance(transform.position, player.transform.position) > 0.6f)
        {
            Debug.Log("time to change to IDLE...");
            stateMachine.ChangeState(((EnemySM)stateMachine).st_idle);
        }
    }

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();

        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);

    }

    public override void Exit()
    {
        base.Exit();
    }
}
